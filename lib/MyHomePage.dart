import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final CalendarController _calendarController = CalendarController();
  @override
  initState() {
    // _calendarController.view = CalendarView.month;
    super.initState();
  }

  List<TimeRegion> _getTimeRegions() {
    final List<TimeRegion> regions = <TimeRegion>[];
    regions.add(TimeRegion(
        startTime: DateTime.now(),
        endTime: DateTime.now().add(Duration(hours: 1)),
        recurrenceRule: 'FREQ=DAILY;INTERVAL=1',
        enablePointerInteraction: false,
        color: Colors.grey.withOpacity(0.2),
        text: 'Break'));

    return regions;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: SizedBox(
          child: SfCalendar(
            specialRegions: _getTimeRegions(),
            // scheduleViewSettings: const ScheduleViewSettings(
            //   appointmentItemHeight: 20,
            // ),
// view: CalendarView.month,
            monthViewSettings: const MonthViewSettings(
                showAgenda: true,
                appointmentDisplayMode: MonthAppointmentDisplayMode.appointment,
                agendaItemHeight: 90,
                agendaViewHeight: 100,
                agendaStyle: AgendaStyle(
                    appointmentTextStyle:
                        TextStyle(fontWeight: FontWeight.bold))),

            // timeSlotViewSettings: const TimeSlotViewSettings(
            //   timelineAppointmentHeight: 100,
            //   minimumAppointmentDuration: Duration(minutes: 30),
            // ),
            weekNumberStyle: const WeekNumberStyle(
              backgroundColor: Colors.deepPurple,
              textStyle: TextStyle(color: Colors.white, fontSize: 15),
            ),
            showWeekNumber: true,
            showNavigationArrow: true,
            cellBorderColor: Colors.purple,
            todayHighlightColor: Colors.red,
            selectionDecoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: Colors.green, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(6)),
              shape: BoxShape.rectangle,
            ),
            firstDayOfWeek: 1,
            dataSource: _getCalendarDataSource(),
            // monthViewSettings:const MonthViewSettings(
            //     appointmentDisplayMode:
            //         MonthAppointmentDisplayMode.appointment),
            // controller: _calendarController,
            showDatePickerButton: true,
            view: CalendarView.timelineWeek,
            onViewChanged: (ViewChangedDetails details) {},
            allowedViews: const [
              CalendarView.day,
              CalendarView.week,
              CalendarView.workWeek,
              CalendarView.month,
              CalendarView.timelineDay,
              CalendarView.timelineWeek,
              CalendarView.timelineWorkWeek,
            ],
          ),
        ),
      ),
    );
  }
}

class DataSource extends CalendarDataSource {
  DataSource(List<Appointment> source) {
    appointments = source;
  }
}

DataSource _getCalendarDataSource() {
  List<Appointment> appointments = <Appointment>[];
  appointments.add(Appointment(
    startTime: DateTime.now(),
    endTime: DateTime.now().add(const Duration(minutes: 10)),
    isAllDay: true,
    subject: 'Meeting',

    color: Colors.deepPurpleAccent,
    // startTimeZone: '',
    // endTimeZone: '',
  ));

  return DataSource(appointments);
}
